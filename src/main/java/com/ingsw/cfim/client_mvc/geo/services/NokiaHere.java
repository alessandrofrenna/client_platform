package com.ingsw.cfim.client_mvc.geo.services;

import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class NokiaHere implements GeoAPI {
  @Override
  public JSONObject getCoordinatesFromAddress(String requestUrl) {
    try {
      RestTemplate request = new RestTemplate();
      ResponseEntity<String> response = request.getForEntity(requestUrl, String.class);
      return new JSONObject(response.getBody());
    } catch (Exception exception) {
      return null;
    }
  }
}