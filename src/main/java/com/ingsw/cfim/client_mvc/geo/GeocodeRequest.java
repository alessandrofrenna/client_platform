package com.ingsw.cfim.client_mvc.geo;

import com.ingsw.cfim.client_mvc.geo.builder.Geocode;
import com.ingsw.cfim.client_mvc.geo.parsers.AddressParser;
import com.ingsw.cfim.client_mvc.geo.parsers.GeoResponseParser;
import com.ingsw.cfim.client_mvc.geo.services.GeoAPI;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

public class GeocodeRequest {

  @Autowired
  private GeoAPI geoAPI;

  private Geocode builder;
  private String endpoint;
  private String apiKey;

  public GeocodeRequest(Geocode geocodeBuilder, String geoEndpoint, String apiKey) {
    this.builder = geocodeBuilder;
    this.endpoint = geoEndpoint;
    this.apiKey = apiKey;
  }

  public GeoJsonPoint getAddressCoordinatesFromAPI(String address) {
    String url = builder.setAPIEndpoint(this.endpoint).setRequestParam("apiKey", this.apiKey)
        .setRequestParam("searchText", address).buildRequestUrl();

    JSONObject json = this.geoAPI.getCoordinatesFromAddress(url);

    if (json == null) {
      return null;
    }

    GeoResponseParser parser = new AddressParser(json);
    return parser.parseLocation().getParsedCoordinates();
  }

}
