package com.ingsw.cfim.client_mvc.controllers;

import com.ingsw.cfim.client_mvc.forms.AddressForm;
import com.ingsw.cfim.client_mvc.geo.GeocodeRequest;
import com.ingsw.cfim.client_mvc.models.Restaurant;

import com.ingsw.cfim.client_mvc.services.RestaurantService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

import javax.validation.Valid;

@Slf4j
@Controller
public class HomeController {
  private GeocodeRequest geoProvider;
  private RestaurantService restaurantService;

  HomeController(RestaurantService svc, GeocodeRequest geoProvider) {
    this.restaurantService = svc;
    this.geoProvider = geoProvider;
  }

  @GetMapping("/")
  public String showHomepage(AddressForm addressForm) {
    return "index";
  }

  @PostMapping("/restaurants")
  public String getRestaurants(@Valid AddressForm addressForm, BindingResult result, Model model) {
    if (result.hasErrors()) {
      return "index";
    }

    try {
      Double distance = 10000d;
      GeoJsonPoint point = this.geoProvider.getAddressCoordinatesFromAPI(addressForm.getAddress());

      if(point == null) {
        model.addAttribute("error", "Indirizzo cercato non valido");
        return "index";
      }

      List<Restaurant> results = restaurantService.getNearestRestaurants(point.getY(), point.getX(), distance);
      model.addAttribute("restaurants", results);
      model.addAttribute("address", addressForm);
      return "restaurants";
    } catch (Exception ex) {
      log.error(ex.toString());
      return "index";
    }
  }

}
