package com.ingsw.cfim.client_mvc.controllers;

import com.ingsw.cfim.client_mvc.models.Restaurant;
import com.ingsw.cfim.client_mvc.services.RestaurantService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/restaurant")
public class RestaurantController {
    private RestaurantService restaurantService;

    RestaurantController(RestaurantService svc) {
        this.restaurantService = svc;
    }

    @GetMapping("/{id}")
    public String showRestaurantpage(@PathVariable("id") ObjectId id, Model model) {
        Restaurant restaurant = restaurantService.getRestaurantInfo(id);
        model.addAttribute("restaurant", restaurant);
        return "restaurant_page";
    }

    public String firstToUpper(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }
}
