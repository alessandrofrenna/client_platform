package com.ingsw.cfim.client_mvc.services;

import com.ingsw.cfim.client_mvc.models.Restaurant;
import org.bson.types.ObjectId;

import java.util.List;

public interface RestaurantService {
    List<Restaurant> getNearestRestaurants(Double longitude, Double latitude, Double distance);
    Restaurant getRestaurantInfo(ObjectId id);
}
