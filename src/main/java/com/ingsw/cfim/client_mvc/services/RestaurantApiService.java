package com.ingsw.cfim.client_mvc.services;

import com.ingsw.cfim.client_mvc.clients.RestaurantsClient;
import com.ingsw.cfim.client_mvc.clients.mappers.NearestQueryMapper;
import com.ingsw.cfim.client_mvc.models.ProductsMap;
import com.ingsw.cfim.client_mvc.models.Restaurant;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RestaurantApiService implements RestaurantService{

    @Autowired
    private RestaurantsClient apiClient;

    @Override
    public List<Restaurant> getNearestRestaurants(Double longitude, Double latitude, Double distance) {
        List<Restaurant> restaurants;
        try{
            NearestQueryMapper params = new NearestQueryMapper(longitude, latitude, distance);
            restaurants = apiClient.nearest(params);
        }catch (NullPointerException ex) {
            restaurants = null;
        }
        return restaurants;

    }

    @Override
    public Restaurant getRestaurantInfo(ObjectId id) {
        Restaurant restaurant = apiClient.getRestaurant(id);
        ProductsMap products = apiClient.restaurantProducts(id);
        restaurant.setProducts(products.getProducts());
        return restaurant;
    }
}
